def fibonacci(n):
    assert n >= 0, 'invalid n'
    if n < 2:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)
