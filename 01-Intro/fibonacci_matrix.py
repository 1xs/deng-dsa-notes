from numpy import matrix


def MatrixPower(mat, n):
    assert n > 0, 'invalid n'
    res = None
    temp = mat
    while True:
        if n & 1:
            if res is None:
                res = temp
            else:
                res = res * temp
        n >>= 1
        if n == 0:
            break
        temp = temp * temp
    return res


def fibonacci(n):
    assert n >= 0, 'invalid n'
    if n < 2:
        return n  # F(0) = 0, F(1) = 1
    mat = matrix([[1, 1], [1, 0]], dtype=object)
    mat = MatrixPower(mat, n - 1)
    return mat[0, 0]
