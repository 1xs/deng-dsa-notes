def fibonacci(n):
    root_Five = 5**0.5
    result = (((1 + root_Five) / 2)**n - ((1 - root_Five) / 2)**n) / root_Five
    return int(result)
