# 第一章 绪论

- [第一章 绪论](#第一章-绪论)
  - [时间复杂度记号](#时间复杂度记号)
  - [典型的复杂度](#典型的复杂度)
  - [算法复杂度分析](#算法复杂度分析)
    - [对数](#对数)
  - [迭代递归](#迭代递归)
    - [斐波那契数列](#斐波那契数列)
      - [递归](#递归)
      - [迭代](#迭代)
      - [矩阵](#矩阵)
      - [通项公式](#通项公式)
    - [Max2](#max2)
    - [总和最大区段](#总和最大区段)
  - [动态规划](#动态规划)
    - [LCS 最长公共子序列](#lcs-最长公共子序列)
  - [局限：缓存，字宽，随机数](#局限缓存字宽随机数)
  - [下界：代数判定树，归约](#下界代数判定树归约)

## 时间复杂度记号

![time-complexity-notation](readme.assets/time-complexity-notation.png)

$`O`$表示对函数上界的估计，意味着最坏情况，通常使用 $`O`$，因为对来说我们更担心最坏情况的出现

$`\Omega`$ 表示最好情况，是对函数下界的估计

$`\Theta`$ 是更为精确的上下界的估计，即存在两个常数分别作用于上下界，而 T(n) 位于中间。

大 $`O`$ 标记下常用时间复杂度

```math
O(1) < O(\log n) < O(n) < O(n \log n) < O(n^2) < O(2^n) < O(n!)
```

二分计算中 $`O(\log n)`$ 的由来：n 个元素，k 次计算

```math
n \cdot  (\frac{1}{2})^{k} = 1 \rightarrow n = 2^k \rightarrow k = \log n
```

## 典型的复杂度

![complexity-comparison](readme.assets/complexity-comparison.png)

算数级数，与末项平方同阶，$`T(n)=1+2+3+\cdots+n=n(n+1)=O(n^2)`$

幂方级数，比幂次高出一阶，$`T_2(n)=1^2+2^2+3^2+\cdots+n^2=\frac{n(n+1)(2n+1)}{6}=O(n^3)`$

几何级数，与末项同阶，$`T_a(n)=a^0+a^1+a^2+\cdots+a^n=\frac{a^{n+1}-1}{a-1}=O(a^n)`$

调和级数 ，$`1+\frac{1}{2}+\frac{1}{3}+\cdots+\frac{1}{n}=\ln n + \gamma + O(\frac{1}{2n})=O(\log n)`$

对数级数，$`\ln n!=O(n \log n)`$

## 算法复杂度分析

![common-complexity-list](readme.assets/common-complexity-list.png)

![complexity-level](readme.assets/complexity-level.png)

常数复杂度较为简单，故不做笔记。

### 对数

教材 P13 页  `countOnes` 函数实现对整数二进制展开中数位 1 总数的统计，有如下解法

1. 检查最低位，若为 1 则计数，此方法复杂度为$`O(\log n)`$，关键部分代码

   ```cpp
    ones += ( 1 & n ); // 检查最低位，若为 1 则计数
    n >>= 1;
   ```

2. 优化，计数（至少有一位为1），清除当前最靠右的 1，复杂度为$`O(countOnes(n))`$，线性正比于数位 1 的实际数目，虽然是线性的，但是对比方法 1，实际跳过了数位为 0 的计算

   ```cpp
   ones++;
   n &= n - 1; // 清除当前最靠右的 1
   ```

3. 再优化，$`O(\log_2 W)，W = O(\log_2 n)`$ 为整数的位宽，即复杂度为$`O(\log \log n)`$

[countOnes2 in cpp](./countOnes2.cpp)

分析

- 在第一次 ROUND 中，以题目 441 为例，右移 1 位
按 1 位分，显然各组都有一个 1，一共 6 个。
110111001 & 01010101010101010101010101010101 + 11011100 & 01010101010101010101010101010101
结果为 100010001 + 1010100 = 101100101
按 2 位分，1 + 1 + 2 + 1 + 1 
观察上面的处理结果，`(n) & MASK(c)` 是将低位，即 `(c_0 c_1)` 中的 c_1 为 1 的保留下来，而 `(n) >> POW(c) & MASK(c)` 则是将高位中的 1 保留下来。后面的步骤同理

- 第二次，右移 2 位
101100101 & 00110011001100110011001100110011 + 1011001 & 00110011001100110011001100110011
100100001 + 10001 = 100110010
按 4 位分，1 + 3 + 2

- 第三次，右移 4 位
100110010 & 00001111000011110000111100001111 + 10011 & 00001111000011110000111100001111
100000010 + 11 = 100000101
按 8 位分，1 + 5

- 第四次，右移 8 位 
100000101 & 00000000111111110000000011111111 + 1 & 00000000111111110000000011111111
101 + 1 = 110
按 16 位分，6

- 第五次，右移 16 位
110 & 00000000000000001111111111111111
110 = 6
已经处理完毕了，依然是 6

> @hwc0919 在 MOOC 的回复 ↓

![hwc0919-analysis](readme.assets/hwc0919-analysis-1585468881960.png)

## 迭代递归

分治策略对应的递推式通常（尽管不总是）形如：$`T(n)=a \cdot T(\frac{n}{b}) + O(f(n))`$

To iterate is human, to recurse divine.

递归不用考虑细节，但是有溢出风险，因此通常实现递归方法后，再去用迭代模拟。

减而治之：递归每深入一层，待求解问题的规模都缩减一个常数，直至最终蜕化为平凡的小（简单）问题。

[Master Theorem](https://zh.wikipedia.org/wiki/主定理)

### 斐波那契数列

$`F(n) = F(n-1)+F(n-2)`$，其中 $`F(0)=0, F(1)=1`$，即该数列由 0 和 1 开始，之后的数字由相邻的前两项相加而得出。

#### 递归

[fibonacci_recurse in python](./fibonacci_recurse.py)

递归方法的时间复杂度为高度为 $`n-1`$ 的不完全二叉树的节点数，所以近似为 $`O(2^n)`$

数学求解方法：

$`T(n) = T(n-1) + T(n-2) \qquad (n>1)`$

设 $`f(n)`$ 为参数为 n 时的时间复杂度 $`f(n) = f(n-1) + f(n-2)`$

转化为求二阶常系数齐次差分方程，设通解为  $`f_{n} = C_{1}f_{n-1} + C_{2}f_{n-2}`$

设有特解 $`f_{n} = \lambda`$，$`\lambda`$ 为非零待定常数，将 $`\lambda`$ 代入方程，易得特征方程 $`\lambda ^{2} = \lambda  + 1`$，则 $`λ = \frac{1\pm \sqrt{5}}{2}`$

再根据 $`f(0) = 0, f(1) = 1.`$ 求出 $`C_{1}`$ 和 $`C_{2}`$ 得出通项公式

```math
f(n) = \frac{1}{\sqrt{5}}[(\frac{1+ \sqrt{5}}{2})^{n} - (\frac{1- \sqrt{5}}{2})^{n}]
```

当 $`n->\infty`$ 时， $`\left |(\frac{1- \sqrt{5}}{2})^{n} \right |<1`$，所以趋于零。

时间复杂度为 $`O((\frac{1+ \sqrt{5}}{2})^{n})`$ ，约等于 $`O(1.618^{n})`$ ，即指数级复杂度 $`O(2^n)`$，递归算法空间复杂度取决于递归的深度，显然为 $`O(n)`$。

#### 迭代

[fibonacci_iterate in python](./fibonacci_iterate.py)

时间复杂度 $`O(n)`$，空间复杂度 $`O(1)`$

https://dsa.cs.tsinghua.edu.cn/~deng/ds/src_link/fibonacci/fib0.cpp.htm

#### 矩阵

$`F(n)`$ 和 $`F(n - 1)`$ 写成一个 2 x 1 的矩阵，然后对其进行变形。

```math
\begin{bmatrix}F_{n}\\F_{n-1}\end{bmatrix}=\begin{bmatrix}F_{n-1}+F_{n-2}\\F_{n-1}\end{bmatrix}=\begin{bmatrix}1\times F_{n-1}+1\times F_{n-2}\\1\times F_{n-1}+0\times F_{n-2}\end{bmatrix}=\begin{bmatrix}1 & 1\\ 1 & 0\end{bmatrix}\times\begin{bmatrix}F_{n-1}\\F_{n-2}\end{bmatrix}=...=\begin{bmatrix}1&1\\1&0\end{bmatrix}^{n-1}\times\begin{bmatrix}F_{1}\\F_{0}\end{bmatrix}=\begin{bmatrix}1&1\\1&0\end{bmatrix}^{n-1}\times\begin{bmatrix}1\\0\end{bmatrix}
```

因此要求 $`F_{n}`$，只要对这个二阶方阵求 $`n - 1`$ 次方，最后取结果方阵第一行第一列的数字就可以了。

等式中的矩阵 $`\begin{bmatrix}1&1\\1&0\end{bmatrix}`$ 被称为斐波那契数列的 Q- 矩阵。

通过 Q- 矩阵，我们可以利用如下公式进行计算：


$`F_{n} = Q^{n-1}_{1,1}`$

如此一来，计算斐波那契数列的问题就转化为了求 $`Q^{n-1}`$ 的问题。

```math
A^{n} = \begin{cases} A\cdot (A^{2})^{\frac{n-1}{2}}, & \text{if $n$ is odd} \\\ (A^{2})^{\frac{n}{2}}, & \text{if $n$ is even} \\ \end{cases}
```

可见时间复杂度满足 $`T(n) = T(n / 2) + O(1)`$
由 [Master 定理](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 可得

时间复杂度 $`O( \log n)`$ ，空间复杂度显然为 $`O(1)`$

[fibonacci_matrix in python](./fibonacci_matrix.py)

#### 通项公式

> https://www.zhihu.com/question/25217301

[fibonacci_general_formula in python](./fibonacci_general_formula.py)

显然，该方法的时空复杂度均为 $`O(1)`$ ， 但使用公式计算的方法由于有大量的浮点运算,在 n 增大时浮点误差不断增大会导致返回结果不正确甚至数据溢出。

| 方法 | 时间复杂度 | 空间复杂度 |
| ---- | ---- | ---- |
| 递归 | $`O(2^{n})`$ | $`O(n)`$ |
| 迭代 | $`O(n)`$ | $`O(1)`$ |
| 矩阵 | $`O(log^{n})`$ | $`O(1)`$ |
| 公式 | $`O(1)`$ | $`O(1)`$ |

### Max2

从 $`O(2n - 3)`$ 到 $`O(n - 1)`$，

再到 $`O(5n/3 - 2)`$，

推导过程简述：

$`T(n) + 2 = 2 * (T(\frac{n}{2}) + 2)`$，令 $`G(n) = T(n) + 2`$，有

$`G(n) = 2G(\frac{n}{2}) = 4G(\frac{n}{4}) = 8G(\frac{n}{8}) = ...`$

不考虑取整，有 $`G(n) = (\frac{n}{3})G(n/(\frac{n}{3}))=(\frac{n}{3})G(3)`$

递归基为 $`G(2)`$ 和 $`G(2)`$ ，取较大者，$`G(n)=(\frac{n}{3})G(3)<=\frac{5n}{3}`$

所以有 $`T(n) = G(n)-2 \le \frac 5 3 \cdot n-2`$

>推导过程 @yuantailing

### 总和最大区段

任给整数序列，找出其中总和最大的区段，经典问题。

[LeetCode 53. Maximum Subarray](https://leetcode-cn.com/problems/maximum-subarray/)

[Solution](https://gitlab.com/1xs/leetcode-solutions/-/tree/master/Algorithms/53.Maximum-Subarray)

## 动态规划

> https://www.zhihu.com/question/23995189/answer/613096905

是否能使用 DP？

通过后无效性以及最优子结构判定

为什么 DP 高效？

自带剪枝，尽量缩小可能解空间

> Introduction to Algorithms, 3rd Edition
```
When developing a dynamic-programming algorithm, we follow a sequence of
four steps:
1. Characterize the structure of an optimal solution.
2. Recursively define the value of an optimal solution.
3. Compute the value of an optimal solution, typically in a bottom-up fashion.
4. Construct an optimal solution from computed information.
```

### LCS 最长公共子序列

> [原演示 Excel 地址](https://www.xuetangx.com/c4x/TsinghuaX/30240184_1X/asset/lcs.xlsx)

[备份下载，Excel 打开后通过按 F9 查看生成的各种情况](./lcs.xlsx)

> 无论是动态规划还是迭代，一般都存在“重复执行一系列运算步骤”的特点，但是动态规划更强调一张“表”的存在，迭代只是拿着有限个数据进行反复运算，直观来说，动态规划过程中你的已知数据是越来越多的，例如算dp(n)时dp(1)到dp(n-1)都可以作为已知数据，而对迭代而言始终只有上一次的迭代结果，例如迭代第n次时你只有第n-1次的结果。有的迭代可以看作动态规划，例如斐波那契数列的求解；有的则泾渭分明，例如01背包问题是动态规划不是迭代，二分查找问题是迭代（如果你没有写成递归的话）不是动态规划

讲义 01-XA2

算法导论 第 15 章

|1143|[Longest Common Subsequence](https://leetcode-cn.com/problems/longest-common-subsequence/) |Medium|

## 局限：缓存，字宽，随机数

缓存

|189|[Rotate Array](https://leetcode-cn.com/problems/rotate-array/)|Easy|

字宽

？

## 下界：代数判定树，归约

CBA (Classification Based on Associations)

- 代数判定树

包含 N 个叶节点的排序算法 ADT，高度不低于

$` log_3 N \geq log_3 n! \\ = log_3 e \cdot  [n \ln n - n + O(\ln n)] \\ = Q(n \cdot  \log n)`$

强记住，推导可能需要看具体数学